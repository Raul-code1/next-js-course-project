This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
yarn
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

---

In order To run dev mode you need to set up mongodb database with docker-compose.yml file.

```
cd ( root-app-folder)

docker-compose up -d

```

MongoDb local URL:
`mongodb://localhost:27017/entriesdb`

---

### Env variables must be configured

Rename the file **.env.template\_** to **_.env_**

### Fill database with mockdata

request seed url **http://localhost:3000/api/seed**
