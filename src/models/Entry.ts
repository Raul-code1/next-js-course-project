import { Schema, models, Model, model } from "mongoose";

import { Entry } from "@/interfaces";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IEntry extends Entry {}

const EntrySchema = new Schema({
  description: {
    type: String,
    required: [true, "Please provide a description"],
  },
  createdAt: {
    type: Number,
    required: true,
  },
  status: {
    type: String,
    enum: ["pending", "in-progress", "finished"],
    default: "pending",
  },
});

const EntryModel: Model<IEntry> = models.Entry || model("Entry", EntrySchema);

export default EntryModel;
