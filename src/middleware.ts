// middleware.ts

import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(req: NextRequest) {
  if (req.nextUrl.pathname.startsWith("/api/entries/")) {
    const id = req.nextUrl.pathname.replace("/api/entries/", "");
    const checkMongoIdRegexp = new RegExp("^[0-9a-fA-F]{24}$");
    if (!checkMongoIdRegexp.test(id)) {
      const url = req.nextUrl.clone();
      // eslint-disable-next-line no-console
      console.log(url);

      url.pathname = "/api/bad-request";
      url.search = "?message=Invalid Id'";
      return NextResponse.rewrite(url);
    }
  }

  return NextResponse.next();
}

export const config = {
  matcher: ["/api/entries/:path*"],
};
