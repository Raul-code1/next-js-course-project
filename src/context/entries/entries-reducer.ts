import { Entry } from "@/interfaces";

import { EntryState } from "./entries-provider";

type ACTION_TYPE =
  | { type: "ADD_ENTRY"; payload: Entry }
  | { type: "SET_ENTRY_STATUS"; payload: Entry }
  | { type: "REFRESH_ENTRIES"; payload: Entry[] };

function entriesReducer(state: EntryState, action: ACTION_TYPE): EntryState {
  switch (action.type) {
    case "ADD_ENTRY":
      return { ...state, entries: [...state.entries, action.payload] };
    case "SET_ENTRY_STATUS":
      return {
        ...state,
        entries: state.entries.map((entry) => {
          if (entry._id === action.payload._id) {
            entry.status = action.payload.status;
            entry.description = action.payload.description;
          }
          return entry;
        }),
      };
    case "REFRESH_ENTRIES":
      return { ...state, entries: [...action.payload] };
    default:
      return state;
  }
}

export default entriesReducer;
