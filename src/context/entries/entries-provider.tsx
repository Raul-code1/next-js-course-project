import { useEffect } from "react";
/* eslint-disable no-console */
import { useReducer } from "react";

import { entriesApi } from "@/api";
import { Entry } from "@/interfaces";

import { EntriesContext } from "./entries-context";
import entriesReducer from "./entries-reducer";

export interface EntryState {
  entries: Entry[];
}

const ENTRY_INITIAL_STATE: EntryState = {
  entries: [],
};

function EntryProvider({ children }: { children: React.ReactNode }) {
  const [state, dispatch] = useReducer(entriesReducer, ENTRY_INITIAL_STATE);

  /* Creating a new entry */
  async function addNewEntry(description: string) {
    const { data } = await entriesApi.post<{ entry: Entry }>("/entries", {
      description,
    });
    dispatch({ type: "ADD_ENTRY", payload: data.entry });
  }

  /* Updating an entry */
  async function setEntryStatus(entry: Entry): Promise<void> {
    const { description, status, _id } = entry;
    try {
      const { data } = await entriesApi.patch<{ entry: Entry }>(
        `/entries/${_id} `,
        {
          description,
          status,
        }
      );

      dispatch({ type: "SET_ENTRY_STATUS", payload: data.entry });
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  }

  /* Getting  entries */
  async function refreshEntries() {
    const { data } = await entriesApi.get<{ entries: Entry[] }>("/entries");
    dispatch({ type: "REFRESH_ENTRIES", payload: data.entries });
    console.log(data.entries);
  }

  useEffect(() => {
    refreshEntries();
  }, []);

  return (
    <EntriesContext.Provider value={{ ...state, addNewEntry, setEntryStatus }}>
      {children}
    </EntriesContext.Provider>
  );
}

export default EntryProvider;
