/* eslint-disable no-unused-vars */
import { createContext, useContext } from "react";

import { Entry } from "@/interfaces";

export interface EntriesInterface {
  entries: Entry[];

  /* Methods */
  addNewEntry: (description: string) => Promise<void>;
  setEntryStatus: (entry: Entry) => void;
}
export const EntriesContext = createContext({} as EntriesInterface);

export function useEntriesContext() {
  return useContext(EntriesContext);
}
