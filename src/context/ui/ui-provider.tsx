import { useReducer } from "react";

import { UiContext } from "./ui-context";
import uiReducer from "./ui-reducer";

export interface UiState {
  sideMenuOpen: boolean;
  isAddingEntry: boolean;
  isDragging: boolean;
}

const INITIAL_STATE: UiState = {
  sideMenuOpen: false,
  isAddingEntry: false,
  isDragging: false,
};

function UiContextProvider({ children }: { children: React.ReactNode }) {
  const [state, dispatch] = useReducer(uiReducer, INITIAL_STATE);

  function closeSideMenu(): void {
    dispatch({ type: "CLOSE_SIDEBAR" });
  }

  function openSideMenu(): void {
    dispatch({ type: "OPEN_SIDEBAR" });
  }

  function setIsAddingEntry(isAdding: boolean): void {
    dispatch({ type: "SET_IS_ADDING_ENTRY", payload: isAdding });
  }

  function startDragging() {
    dispatch({ type: "START_DRAGGING" });
  }

  function endDragging() {
    dispatch({ type: "END_DRAGGING" });
  }

  return (
    <UiContext.Provider
      value={{
        /* State */
        ...state,
        /* methods */
        closeSideMenu,
        openSideMenu,
        setIsAddingEntry,
        startDragging,
        endDragging,
      }}
    >
      {children}
    </UiContext.Provider>
  );
}

export default UiContextProvider;
