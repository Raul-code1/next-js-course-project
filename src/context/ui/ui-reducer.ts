import { UiState } from "./ui-provider";

type ACTION_TYPE =
  | { type: "OPEN_SIDEBAR" }
  | { type: "CLOSE_SIDEBAR" }
  | { type: "SET_IS_ADDING_ENTRY"; payload: boolean }
  | { type: "START_DRAGGING" }
  | { type: "END_DRAGGING" };

function uiReducer(state: UiState, action: ACTION_TYPE) {
  switch (action.type) {
    case "OPEN_SIDEBAR":
      return { ...state, sideMenuOpen: true };
    case "CLOSE_SIDEBAR":
      return { ...state, sideMenuOpen: false };
    case "SET_IS_ADDING_ENTRY":
      return { ...state, isAddingEntry: action.payload };
    case "START_DRAGGING":
      return { ...state, isDragging: true };
    case "END_DRAGGING":
      return { ...state, isDragging: false };

    default:
      return state;
  }
}

export default uiReducer;
