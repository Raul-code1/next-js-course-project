/* eslint-disable no-unused-vars */
import { createContext, useContext } from "react";

export interface UiContextInterface {
  sideMenuOpen: boolean;
  isAddingEntry: boolean;
  isDragging: boolean;
  /* Methods */
  closeSideMenu: () => void;
  openSideMenu: () => void;
  setIsAddingEntry: (isAdding: boolean) => void;
  startDragging: () => void;
  endDragging: () => void;
}

export const UiContext = createContext({} as UiContextInterface);

export function useAppUiContext() {
  return useContext(UiContext);
}
