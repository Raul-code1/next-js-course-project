import {
  Box,
  Drawer,
  Typography,
  List,
  ListItem,
  ListItemIcon,
} from "@mui/material";

import { useAppUiContext } from "@/context/ui";

const menuItems: string[] = ["Home", "About"];

export default function SideBar() {
  const { sideMenuOpen, closeSideMenu } = useAppUiContext();

  return (
    <Drawer anchor="left" open={sideMenuOpen} onClose={closeSideMenu}>
      <Box sx={{ padding: "5px 10px" }}>
        <Typography variant="h4">Menu</Typography>
      </Box>
      <List>
        {menuItems.map((item, index) => (
          <ListItem key={item}>
            <ListItemIcon>{index % 2 ? "Something" : "Something"}</ListItemIcon>
          </ListItem>
        ))}
      </List>
    </Drawer>
  );
}
