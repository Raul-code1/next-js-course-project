import { List, Paper } from "@mui/material";

import { useEntriesContext } from "@/context/entries";
import { useAppUiContext } from "@/context/ui";
import { EntryStatus } from "@/interfaces";

import EntryCard from "./EntryCard";

type Props = {
  status: EntryStatus;
};

export default function EntryList({ status }: Props) {
  const { entries, setEntryStatus } = useEntriesContext();
  const { isDragging, endDragging } = useAppUiContext();

  function onDropEntry(event: React.DragEvent<HTMLDivElement>) {
    const id = event.dataTransfer.getData("text");
    const entry = entries.find((entry) => entry._id === id);
    if (!entry) return;
    entry.status = status;
    setEntryStatus(entry);
    endDragging();
  }

  function allowDrop(event: React.DragEvent<HTMLDivElement>) {
    event.preventDefault();
  }

  return (
    <div onDrop={onDropEntry} onDragOver={allowDrop}>
      <Paper
        sx={{
          height: "calc(100vh- 250px)",
          backgroundColor: "transparent",
          padding: "3px 5px",
        }}
      >
        <List sx={{ opacity: isDragging ? 0.2 : 1, transition: "all 0.3s" }}>
          {entries.map(
            (entry) =>
              entry.status === status && (
                <EntryCard key={entry._id} entry={entry} />
              )
          )}
        </List>
      </Paper>
    </div>
  );
}
