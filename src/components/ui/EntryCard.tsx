import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";

import { useAppUiContext } from "@/context/ui";
import { Entry } from "@/interfaces";

type Props = {
  entry: Entry;
};

export default function EntryCard({ entry }: Props) {
  const router = useRouter();

  const { startDragging, endDragging } = useAppUiContext();

  function onDragStart(event: React.DragEvent<HTMLDivElement>) {
    event.dataTransfer.setData("text", entry._id);
    startDragging();
  }

  function onDragEnd() {
    endDragging();
  }

  function onClick() {
    router.push(`/entries/${entry._id}`);
  }

  return (
    <Card
      sx={{ marginBottom: 1 }}
      draggable={true}
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onClick={onClick}
    >
      <CardActionArea>
        <CardContent>
          <Typography sx={{ whiteSpace: "pre-line" }}>
            {entry.description}
          </Typography>
          <CardActions sx={{ display: "flex", justifyContent: "flex-end" }}>
            <Typography variant="body2">About 30 minutes ago</Typography>
          </CardActions>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
