import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import SaveOutlineIcon from "@mui/icons-material/SaveOutlined";
import { Button, Box, TextField } from "@mui/material";
import { useState } from "react";

import { useEntriesContext } from "@/context/entries";
import { useAppUiContext } from "@/context/ui";

export default function NewEntry() {
  const { addNewEntry } = useEntriesContext();
  const { isAddingEntry, setIsAddingEntry } = useAppUiContext();
  const [inputValue, setInputValue] = useState<string>("");
  const [touched, setTouched] = useState<boolean>(false);

  function onSave(): void {
    if (!inputValue || inputValue.length === 0) return;
    setIsAddingEntry(false);
    setTouched(false);
    addNewEntry(inputValue);
    setInputValue("");
  }

  return (
    <Box sx={{ marginBottom: 2, paddingX: 2 }}>
      {isAddingEntry ? (
        <>
          <TextField
            fullWidth
            sx={{ marginTop: 2, marginBottom: 1 }}
            placeholder="New entry"
            autoFocus
            multiline
            label="New Entry"
            value={inputValue}
            error={inputValue.length <= 0 && touched}
            onChange={(event) => setInputValue(event.target.value)}
            helperText={inputValue.length <= 0 && touched && "Required"}
            onBlur={() => setTouched(true)}
          />
          <h5>NewEntry</h5>
          <Box display="flex" justifyContent="space-between">
            <Button
              variant="text"
              color="error"
              onClick={() => {
                setIsAddingEntry(false), setTouched(false);
              }}
            >
              Exit
            </Button>
            <Button
              variant="outlined"
              color="primary"
              endIcon={<SaveOutlineIcon />}
              onClick={onSave}
            >
              Save
            </Button>
          </Box>
        </>
      ) : (
        <Button
          fullWidth
          variant="outlined"
          startIcon={<AddCircleOutlineOutlinedIcon />}
          onClick={() => setIsAddingEntry(true)}
        >
          Add new entry
        </Button>
      )}
    </Box>
  );
}
