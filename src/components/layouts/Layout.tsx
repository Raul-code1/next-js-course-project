import { Box } from "@mui/material";
import Head from "next/head";

import { NavBar, SideBar } from "../ui";

type Props = {
  children: React.ReactNode;
  title: string;
};

export default function Layout({ children, title = "OpenJira App" }: Props) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Head>
        <title>{title}</title>
      </Head>
      <NavBar />
      <SideBar />
      <Box sx={{ padding: "10px 20px" }}>{children}</Box>
    </Box>
  );
}
