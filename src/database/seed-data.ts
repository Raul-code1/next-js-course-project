/* //*If you want to type this seed data, feel free  */
export const seedData = {
  entries: [
    {
      description: " Done: Some description",
      createdAt: Date.now(),
      status: "finished",
    },
    {
      description: "In progress: lorem10lorem10",
      createdAt: Date.now() - 1000000,
      status: "in-progress",
    },
    {
      description: " To do: lorem10lorem10lorem",
      createdAt: Date.now() - 10000,
      status: "pending",
    },
  ],
};
