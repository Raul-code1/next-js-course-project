import { connect, connections, disconnect } from "mongoose";

/*
 *0= disconnected
 *1= connected
 *2= connecting
 *3= disconnecting
 */

const mongoConnection = {
  isConnected: 0,
};

export async function connectDb() {
  if (mongoConnection.isConnected) {
    // eslint-disable-next-line no-console
    console.log("Database connected");
    return;
  }

  if (connections.length > 0) {
    mongoConnection.isConnected = connections[0].readyState;
    await disconnect();
  }

  await connect(process.env.MONGO_URI as string);

  mongoConnection.isConnected = 1;
  // eslint-disable-next-line no-console
  console.log(`Database connected successfully   `);
}

export async function disconnectDb() {
  if (process.env.NODE_ENV === "development") return;

  if (mongoConnection.isConnected === 0) return;
  // eslint-disable-next-line no-console
  console.log(`Database disconnected successfully   `);
  await disconnect();
}
