import { isValidObjectId } from "mongoose";

import { Entry, IEntry } from "@/models";

import { db } from ".";

export async function getEntryById(id: string): Promise<IEntry | null> {
  if (!isValidObjectId(id)) return null;

  await db.connectDb();

  const entry = await Entry.findById(id).lean();

  await db.disconnectDb();

  if (entry && entry._id) {
    entry._id = entry._id.toString();
  }

  return entry;
}
