import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { useState } from "react";
import { toast } from "react-toastify";

import { Layout } from "@/components/layouts";
import { useEntriesContext } from "@/context/entries";
import { getEntryById } from "@/database/dbEntries";
import { Entry, EntryStatus } from "@/interfaces";

const validStatus: EntryStatus[] = ["pending", "in-progress", "finished"];

type Props = {
  entry: Entry;
};

export default function EntryPage({ entry }: Props) {
  const { setEntryStatus } = useEntriesContext();
  const router = useRouter();

  const [inputValue, setInputValue] = useState<string>(entry.description);
  const [status, setStatus] = useState<EntryStatus>(entry.status);

  function onStatusChange(event: React.ChangeEvent<HTMLInputElement>) {
    setStatus(event.target.value as EntryStatus);
  }

  function onSave() {
    if (!inputValue) return toast.error("Please provide a description");

    const updatedEntry: Entry = {
      ...entry,
      description: inputValue,
      status,
    };

    setEntryStatus(updatedEntry);
    toast.success("Entry Updated  ");
    router.push("/");
  }

  return (
    <Layout title="Entry page ">
      <Grid container justifyContent="center" sx={{ marginTop: 2 }}>
        <Grid item xs={12} sm={8} md={6}>
          <Card>
            <CardHeader title="Entry" subheader={`Created ... minutes ago`} />
            <CardContent>
              <TextField
                sx={{ marginTop: 2, marginBottom: 1 }}
                fullWidth
                placeholder="New Entry"
                multiline
                label="Update Entry"
                value={inputValue}
                onChange={(event) => setInputValue(event.target.value)}
              />
              <FormControl>
                <FormLabel>State: </FormLabel>
                <RadioGroup row onChange={onStatusChange} value={status}>
                  {validStatus.map((option) => {
                    return (
                      <FormControlLabel
                        key={option}
                        value={option}
                        control={<Radio />}
                        label={option}
                      />
                    );
                  })}
                </RadioGroup>
              </FormControl>
            </CardContent>
            <CardActions>
              <Button
                startIcon={<SaveOutlinedIcon />}
                variant="contained"
                fullWidth
                onClick={onSave}
              >
                Save
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const { id } = params as { id: string };

  const entry = await getEntryById(id);

  if (!entry) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      entry,
    },
  };
};
