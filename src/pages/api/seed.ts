// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import { seedData } from "@/database/seed-data";
import { Entry } from "@/models";

import { db } from "../../database";

type Data = {
  message: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (process.env.NODE_ENV === "production") {
    return res.status(400).json({ message: "Not allowed to this service" });
  }
  await db.connectDb();
  await Entry.deleteMany();
  await Entry.insertMany(seedData.entries);
  await db.disconnectDb();

  res.status(200).json({ message: "Process successfully" });
}
