import { NextApiRequest, NextApiResponse } from "next";

import { db } from "@/database";
import { Entry } from "@/models";
import { IEntry } from "@/models";

type Data =
  | {
      message: string;
    }
  | { entries: IEntry[] }
  | { entry: IEntry };

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  switch (req.method) {
    case "GET":
      return getEntries(res);
    case "POST":
      return createEntry(req, res);
    default:
      return res.status(400).json({ message: "Endpoint does not exist" });
  }
}

async function getEntries(res: NextApiResponse<Data>) {
  await db.connectDb();
  const entries = await Entry.find({}).sort({ createdAt: -1 });
  await db.disconnectDb();
  res.status(200).json({ entries });
}

async function createEntry(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { description = "" } = req.body;
  const newEntry = new Entry({ description, createdAt: Date.now() });

  try {
    await db.connectDb();
    await newEntry.save();
    await db.disconnectDb();
    return res.status(201).json({ entry: newEntry });
  } catch (error) {
    await db.disconnectDb();
    // eslint-disable-next-line no-console
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
}



