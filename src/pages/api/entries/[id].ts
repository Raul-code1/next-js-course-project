import { isValidObjectId } from "mongoose";
import { NextApiResponse, NextApiRequest } from "next";

import { db } from "@/database";
import { Entry, IEntry } from "@/models";

type Data = { message: string } | { entry: IEntry };

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const { id } = req.query;

  if (!isValidObjectId(id)) {
    return res.status(400).json({ message: "Invalid Id" });
  }

  switch (req.method) {
    case "PATCH":
      return updateTask(req, res);
    case "GET":
      return getEntry(req, res);
    default:
      return res.status(400).json({ message: "Method does not exist" });
  }
}

async function updateTask(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { id } = req.query;

  await db.connectDb();

  const entryToUpdate = await Entry.findById(id);

  if (!entryToUpdate) {
    await db.disconnectDb();
    return res.status(400).json({ message: `Not entry found with id: ${id}` });
  }
  const {
    description = entryToUpdate.description,
    status = entryToUpdate.status,
  } = req.body;

  try {
    const updatedEntry = await Entry.findByIdAndUpdate(
      { _id: id },
      { description, status },
      { new: true, runValidators: true }
    );
    res.status(200).json({ entry: updatedEntry! });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (error: any) {
    await db.disconnectDb();
    res.status(400).json({ message: error.errors.status.message });
  }
}

async function getEntry(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { id } = req.query;

  await db.connectDb();

  const entry = await Entry.findById(id);

  if (!entry) {
    await db.disconnectDb();
    return res.status(400).json({ message: `Not entry found with id: ${id}` });
  }
  return res.status(200).json({ entry: entry });
}
